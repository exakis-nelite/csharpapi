using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using app.Models;
using Newtonsoft.Json.Linq;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace app
{
    public class CalculationEngine
    {
        public CalculsContext _context = new CalculsContext();
        private static readonly Mutex mut = new Mutex();
        private readonly SendtoBroker sender = new SendtoBroker();
        private readonly IDictionary<int, string> threads_data = new Dictionary<int, string>();

        public void startEngine()
        {
            var t_hourly = new Thread(() => BarChartCalcul("hour"));
            var t_daily = new Thread(() => BarChartCalcul("day"));
            var t_weekly = new Thread(() => BarChartCalcul("week"));
            var t_monthly = new Thread(() => BarChartCalcul("month"));
            var t_fly_chart = new Thread(() => FlyChartCalcul("month"));
            var t_pie_chart = new Thread(PieChartCalcul);

            t_hourly.Start();
            t_daily.Start();
            t_weekly.Start();
            t_monthly.Start();
            t_pie_chart.Start();
            //t_fly_chart.Start();

            var BrokerListener = new Thread(this.BrokerListener);

            BrokerListener.Start();
        }

        public void BrokerListener()
        {
            string getEnv = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            var factory = new ConnectionFactory();

            if (getEnv == "production")
            {
                factory = new ConnectionFactory() { HostName = "35.242.155.183", Port = 32439, UserName="guest", Password="guest", VirtualHost = "/" };
            }
            if (getEnv == "development")
            {
                factory = new ConnectionFactory() { HostName = "rabbit1", Port = 5672, UserName="guest", Password="guest", VirtualHost = "/" };
            }
            if (getEnv == "gitlab")
            {
                factory = new ConnectionFactory() { HostName = "rabbitmq", Port = 5672, UserName="guest", Password="guest", VirtualHost = "/" };
            }
            
            using(var connection = factory.CreateConnection())
            using(var channel = connection.CreateModel())
            {
                channel.QueueDeclare("CALCULATION_ENGINE_DATA", true, false, false, null);

                var consumer = new EventingBasicConsumer(channel);

                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    var message = Encoding.UTF8.GetString(body);
                    Console.WriteLine(" [x] Received an answer, {0}", message);

                    var json_answer = JObject.Parse(message);

                    var str = "{ \"data\" : " + json_answer["data"].ToString() + "}";
                    threads_data[(int) json_answer["threadId"]] = str;
                };
                channel.BasicConsume("CALCULATION_ENGINE_DATA", true, consumer);

                while (true) Console.ReadLine();
            }
        }

        public void BarChartCalcul(string period)
        {
            CalculsType calculsType;
            string type_name;
            DateTime start_date;
            string scale;

            switch (period)
            {
                case "hour":
                    type_name = "avg_hour";
                    scale = "10 sec";
                    break;
                case "day":
                    type_name = "avg_day";
                    scale = "5 min";
                    break;
                case "week":
                    type_name = "avg_week";
                    scale = "10 min";
                    break;
                case "month":
                    type_name = "avg_month";
                    scale = "20 min";
                    break;
                default:
                    type_name = "avg_hour";
                    scale = "10 sec";
                    break;
            }

            mut.WaitOne();

            try
            {
                calculsType = _context.CalculsType.Where(c => c.name == type_name).FirstOrDefault();
                var calcul_name = calculsType.name;
            }
            catch (Exception f)
            {
                calculsType = new CalculsType();
                calculsType.name = type_name;
                _context.CalculsType.Add(calculsType);
                _context.SaveChanges();

                calculsType = _context.CalculsType.Where(c => c.name == type_name).FirstOrDefault();
            }
            
            mut.ReleaseMutex();

            while (true)
            {
                mut.WaitOne();
                
                var cat_list = _context.Category.ToList();

                mut.ReleaseMutex();

                foreach (var cat_item in cat_list)
                {
                    if (cat_item.name != "no cat")
                    {
                        IDictionary<int, int> datas_per_device = new Dictionary<int, int>();
                        IDictionary<int, int> datas_total = new Dictionary<int, int>();
                        IDictionary<int, int> devices_users = new Dictionary<int, int>();
                        IDictionary<int, string> devices_names = new Dictionary<int, string>();
                        
                        switch (period)
                        {
                            case "hour":
                                start_date = DateTime.Now.AddHours(-1);
                                break;
                            case "day":
                                start_date = DateTime.Now.AddDays(-1);
                                break;
                            case "week":
                                start_date = DateTime.Now.AddDays(-7);
                                break;
                            case "month":
                                start_date = DateTime.Now.AddMonths(-1);
                                break;
                            default:
                                start_date = DateTime.Now.AddHours(-1);
                                break;
                        }

                        mut.WaitOne();

                        threads_data[Thread.CurrentThread.ManagedThreadId] = "";
                        
                        mut.ReleaseMutex();

                        var json_querry = JSONQuerryConstructor
                        (cat_item.name, start_date, DateTime.Now, "city", scale,
                            Thread.CurrentThread.ManagedThreadId, "bar_chart", "city");

                        sender.publish(json_querry, "CALCULATION_ENGINE_REQUESTS");

                        while (threads_data[Thread.CurrentThread.ManagedThreadId] == "") Thread.Sleep(1000);

                        var users_data = JObject.Parse(threads_data[Thread.CurrentThread.ManagedThreadId]);

                        foreach (var child in users_data["data"].Children())
                        {
                            try
                            {
                                datas_per_device[(int) child["device"]["id"]] += (int) child["value"];
                                datas_total[(int) child["device"]["id"]]++;
                                devices_users[(int) child["device"]["id"]] = (int) child["device"]["user"]["id"];
                                devices_names[(int) child["device"]["id"]] = child["device"]["name"].ToString();
                            }
                            catch (Exception e)
                            {
                                datas_per_device[(int) child["device"]["id"]] = 0;
                                datas_total[(int) child["device"]["id"]] = 0;
                                
                                datas_per_device[(int) child["device"]["id"]] +=(int) child["value"];
                                datas_total[(int) child["device"]["id"]]++;
                                
                                devices_users[(int) child["device"]["id"]] = (int) child["device"]["user"]["id"];
                                devices_names[(int) child["device"]["id"]] = child["device"]["name"].ToString();
                            }
                        }

                        mut.WaitOne();

                        foreach (var data in datas_per_device)
                        {
                            float avg = (float)data.Value / (float)datas_total[data.Key];
                            
                            var calcul = _context.Calculs
                                .Where(c => c.Tag.name == "device")
                                .Where(c => c.Category == cat_item)
                                .Where(c => c.id_user == devices_users[data.Key])
                                .FirstOrDefault(s => s.CalculsType == calculsType);
                            
                            try
                            {
                                calcul.data = avg.ToString();
                                calcul.start_date = start_date;
                                calcul.end_date = DateTime.Now;
                            }
                            catch (Exception e)
                            {
                                calcul = new Calculs();

                                calcul.data = avg.ToString();
                                calcul.CalculsType = calculsType;
                                calcul.start_date = start_date;
                                calcul.end_date = DateTime.Now;
                                calcul.devices = data.Key.ToString();
                                calcul.Tag = _context.Tag.FirstOrDefault(t => t.name == "device");
                                calcul.id_user = devices_users[data.Key];
                                calcul.Category = cat_item;
                                calcul.device_name = devices_names[data.Key];

                                _context.Calculs.Add(calcul);
                            }
                        }

                        _context.SaveChanges();
                        
                        mut.ReleaseMutex();

                        Thread.Sleep(1000);
                    }
                }
                
                switch (period)
                {
                    case "hour":
                        Thread.Sleep(30000);
                        break;
                    case "day":
                        Thread.Sleep(300000);
                        break;
                    case "week":
                        Thread.Sleep(3600000);
                        break;
                    case "month":
                        Thread.Sleep(43200000);
                        break;
                    default:
                        Thread.Sleep(15000);
                        break;
                }
            }
        }

        public string JSONQuerryConstructor(string category, DateTime start_date, DateTime end_date, string groupby,
            string scale, int thread_id, string chart, string tag)
        {
            var answer = "";
            dynamic json_querry = new JObject();

            json_querry.chart_type = chart;
            json_querry.category = category;
            json_querry.start_date = start_date.ToString("yyyy-MM-dd hh:mm:ss");
            json_querry.end_date = end_date.ToString("yyyy-MM-dd hh:mm:ss");
            json_querry.group_by = groupby;
            json_querry.tag = tag;
            json_querry.scale = scale;
            json_querry.thread_id = thread_id;

            answer = json_querry.ToString();

            return answer;
        }

        public void PieChartCalcul()
        {
            CalculsType calcul_type;
            Tag tag;
            Category category;

            mut.WaitOne();

            try
            {
                calcul_type = _context.CalculsType.FirstOrDefault(c => c.name == "pie_chart");
                var type_name = calcul_type.name;
            }
            catch (Exception e)
            {
                calcul_type = new CalculsType();
                calcul_type.name = "pie_chart";
                _context.CalculsType.Add(calcul_type);
                _context.SaveChanges();

                calcul_type = _context.CalculsType.FirstOrDefault(c => c.name == "pie_chart");
            }

            try
            {
                tag = _context.Tag.FirstOrDefault(c => c.name == "user");
                var tag_name = tag.name;
            }
            catch (Exception e)
            {
                tag = new Tag();
                tag.name = "user";
                _context.Tag.Add(tag);
                _context.SaveChanges();

                tag = _context.Tag.FirstOrDefault(c => c.name == "user");
            }

            mut.ReleaseMutex();

            while (true)
            {
                mut.WaitOne();

                threads_data[Thread.CurrentThread.ManagedThreadId] = "";
                        
                mut.ReleaseMutex();

                var json_querry = JSONQuerryConstructor
                ("null", DateTime.Now, DateTime.Now, "null", "null",
                    Thread.CurrentThread.ManagedThreadId, "pie_chart", "null");

                sender.publish(json_querry, "CALCULATION_ENGINE_REQUESTS");

                while (threads_data[Thread.CurrentThread.ManagedThreadId] == "") Thread.Sleep(1000);

                var users_data = JObject.Parse(threads_data[Thread.CurrentThread.ManagedThreadId]);
                
                IDictionary<int, IDictionary<string, int>> users_dictionnary = new Dictionary<int, IDictionary<string, int>>();

                foreach (var child in users_data["data"])
                {
                    try
                    {
                        var devices_dictionnary = users_dictionnary[(int) child["user"]["id"]];

                        devices_dictionnary["total"] += 1;
                        if (devices_dictionnary.ContainsKey(child["category"]["slug"].ToString()))
                        {
                            devices_dictionnary[child["category"]["slug"].ToString()] += 1;
                        }
                        else
                        {
                            devices_dictionnary[child["category"]["slug"].ToString()] = 1;
                        }
                        
                    }
                    catch (Exception e)
                    {
//                        try
//                        {
//                            users_dictionnary[(int) child["user"]["id"]] = new Dictionary<string, int>();
//
//                            var devices_dictionnary = users_dictionnary[(int) child["user"]["id"]];
//
//                            devices_dictionnary["total"] = 1;
//                            devices_dictionnary[child["category"]["slug"].ToString()] = 1;
//                        }catch (Exception r){
//                            Console.WriteLine(r);
//                        }
                    }
                }

                mut.WaitOne();

                foreach (var data in users_dictionnary)
                {
                    var dictionnary = data.Value;

                    float number_of_devices = dictionnary["total"];

                    foreach (var devices in dictionnary)
                    {
                        if (devices.Key != "total")
                        {
                            var cat = _context.Category.FirstOrDefault(c => c.name == devices.Key);
                            
                            var calcul = _context.Calculs.Where(c => c.Category == cat)
                                .Where(c => c.id_user == data.Key)
                                .FirstOrDefault(c => c.CalculsType == calcul_type);   
                            
                            try
                            {
                                calcul.data = devices.Value.ToString();
                            }
                            catch (Exception e)
                            {
                                calcul = new Calculs();

                                calcul.data = devices.Value.ToString();
                                calcul.CalculsType = calcul_type;
                                calcul.start_date = DateTime.Now;
                                calcul.end_date = DateTime.Now;
                                calcul.devices = "";
                                calcul.Tag = _context.Tag.FirstOrDefault(t => t.name == "device");
                                calcul.id_user = data.Key;
                                calcul.Category = cat;
                                calcul.device_name = "";

                                _context.Calculs.Add(calcul);
                            }
                        }
                    }
                }

                _context.SaveChanges();
                
                mut.ReleaseMutex();

                Thread.Sleep(15000);
            }
        }

        public void FlyChartCalcul(string period)
        {
            CalculsType calculsType;
            string type_name;
            DateTime start_date;
            string scale;

            switch (period)
            {
                case "hour":
                    type_name = "avg_hour";
                    scale = "10 sec";
                    break;
                case "day":
                    type_name = "avg_day";
                    scale = "5 min";
                    break;
                case "week":
                    type_name = "avg_week";
                    scale = "10 min";
                    break;
                case "month":
                    type_name = "avg_month";
                    scale = "20 min";
                    break;
                default:
                    type_name = "avg_hour";
                    scale = "10 sec";
                    break;
            }

            mut.WaitOne();

            try
            {
                calculsType = _context.CalculsType.Where(c => c.name == type_name).FirstOrDefault();
                var calcul_name = calculsType.name;
            }
            catch (Exception f)
            {
                calculsType = new CalculsType();
                calculsType.name = type_name;
                _context.CalculsType.Add(calculsType);
                _context.SaveChanges();

                calculsType = _context.CalculsType.Where(c => c.name == type_name).FirstOrDefault();
            }
            
            mut.ReleaseMutex();

            while (true)
            {
                mut.WaitOne();
                
                var cat_list = _context.Category.ToList();

                mut.ReleaseMutex();

                foreach (var cat_item in cat_list)
                {
                    if (cat_item.name != "no cat")
                    {
                        IDictionary<int, IDictionary<string, int>> datas_per_device = new Dictionary<int, IDictionary<string, int>>();
                        
                        switch (period)
                        {
                            case "hour":
                                start_date = DateTime.Now.AddHours(-1);
                                break;
                            case "day":
                                start_date = DateTime.Now.AddDays(-1);
                                break;
                            case "week":
                                start_date = DateTime.Now.AddDays(-7);
                                break;
                            case "month":
                                start_date = DateTime.Now.AddMonths(-1);
                                break;
                            default:
                                start_date = DateTime.Now.AddHours(-1);
                                break;
                        }

                        mut.WaitOne();

                        threads_data[Thread.CurrentThread.ManagedThreadId] = "";
                        
                        mut.ReleaseMutex();

                        var json_querry = JSONQuerryConstructor
                        (cat_item.name, start_date, DateTime.Now, "city", scale,
                            Thread.CurrentThread.ManagedThreadId, "fly_chart", "city");

                        sender.publish(json_querry, "CALCULATION_ENGINE_REQUESTS");

                        while (threads_data[Thread.CurrentThread.ManagedThreadId] == "") Thread.Sleep(1000);

                        var users_data = JObject.Parse(threads_data[Thread.CurrentThread.ManagedThreadId]);

                        foreach (var child in users_data["data"].Children())
                        {
                            
                        }

                        /*mut.WaitOne();

                        foreach (var data in datas_per_device)
                        {
                            float avg = (float)data.Value / (float)datas_total[data.Key];
                            
                            var calcul = _context.Calculs
                                .Where(c => c.Tag.name == "device")
                                .Where(c => c.Category == cat_item)
                                .Where(c => c.id_user == devices_users[data.Key])
                                .FirstOrDefault(s => s.CalculsType == calculsType);
                            
                            try
                            {
                                calcul.data = avg.ToString();
                                calcul.start_date = start_date;
                                calcul.end_date = DateTime.Now;
                            }
                            catch (Exception e)
                            {
                                calcul = new Calculs();

                                calcul.data = avg.ToString();
                                calcul.CalculsType = calculsType;
                                calcul.start_date = start_date;
                                calcul.end_date = DateTime.Now;
                                calcul.devices = data.Key.ToString();
                                calcul.Tag = _context.Tag.FirstOrDefault(t => t.name == "device");
                                calcul.id_user = devices_users[data.Key];
                                calcul.Category = cat_item;
                                calcul.device_name = devices_names[data.Key];

                                _context.Calculs.Add(calcul);
                            }
                        }

                        _context.SaveChanges();
                        
                        mut.ReleaseMutex();*/

                        Thread.Sleep(1000);
                    }
                }
                
                switch (period)
                {
                    case "hour":
                        Thread.Sleep(30000);
                        break;
                    case "day":
                        Thread.Sleep(300000);
                        break;
                    case "week":
                        Thread.Sleep(3600000);
                        break;
                    case "month":
                        Thread.Sleep(30000);
                        break;
                    default:
                        Thread.Sleep(15000);
                        break;
                }
            }
        }
    }
}