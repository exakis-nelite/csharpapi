using System;
using Microsoft.EntityFrameworkCore;

namespace app.Models
{
    public class CalculsContext : DbContext
    {
        public DbSet<Calculs> Calculs { get; set; }
        public DbSet<CalculsType> CalculsType { get; set; }
        public DbSet<Tag> Tag { get; set; }
        public DbSet<Category> Category { get; set; }

        public CalculsContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        { 
            string getEnv = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            switch (getEnv)
            {
                case "production" :
                    optionsBuilder.UseSqlServer(@"Server=mssql;Database=CalculatedMetrics;User ID=SA;Password=Exiakis55");
                    break;
                case "development" :
                    optionsBuilder.UseSqlServer(@"Server=mssql-server-linux;Database=CalculatedMetrics;User ID=SA;Password=Exiakis55");
                    break;
                case "gitlab" :
                    optionsBuilder.UseSqlServer(@"Server=microsoft/mssql-server-linux;Database=CalculatedMetrics;User ID=SA;Password=Exiakis55");
                    break;
                default:
                    optionsBuilder.UseSqlServer(@"Server=mssql-server-linux;Database=CalculatedMetrics;User ID=SA;Password=Exiakis55");
                    break;
                
            }
            optionsBuilder.EnableSensitiveDataLogging();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            modelBuilder.Entity<Tag>().HasData(
                new Tag { id = 1, name = "device" },
                new Tag { id = 2, name = "city" },
                new Tag { id = 3, name = "region" });
            
            modelBuilder.Entity<Category>().HasData(
                new Category { id = 1, name = "temperatureSensor" },
                new Category { id = 2, name = "humiditySensor" },
                new Category { id = 3, name = "brightnessSensor" },
                new Category { id = 4, name = "no cat" });
        }
    }
}