using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace app.Models
{
    public class Calculs
    {
        [Key]
        public int id { get; set; }
        public string devices { get; set; }
        public string data { get; set; }
        public DateTime start_date { get; set; }
        public DateTime end_date { get; set; }
        
        public int CalculsTypesId { get; set; }
        public int CategoryId { get; set; }
        public int TagId { get; set; }
        
        [ForeignKey("CalculsTypesId")]
        public virtual CalculsType CalculsType { get; set; }
        
        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }
        
        [ForeignKey("TagId")]
        public virtual Tag Tag { get; set; }
        public int id_user { get; set; }
        public string device_name { get; set; }
    }
}