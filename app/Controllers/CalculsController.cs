using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using app.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace app.Controllers
{
    [Route("api/calculs")]
    [ApiController]
    public class CalculsController : Controller
    {
        private readonly CalculsContext _context;

        public CalculsController(CalculsContext context)
        {
            _context = context;
        }
        
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Calculs>>> GetCalculs()
        {
            return await _context.Calculs.ToListAsync();
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<Calculs>> GetCalcul(int id)
        {
            var calcul = await _context.Calculs.FindAsync(id);

            if (calcul == null)
            {
                return NotFound();
            }

            return calcul;
        }

        [HttpGet("bar_chart/{id_user}/{length}/{tag}/{cat}")]
        public async Task<ActionResult<IEnumerable<Calculs>>> GetBarChart(int id_user, string length, string tag, string cat)
        {
            var calculs = _context.Calculs
                .Where(c => c.Tag.name == tag)
                .Where(c => c.Category.name == cat)
                .Where(c => c.id_user == id_user)
                .Where(s => s.CalculsType.name == length)
                .ToList();

            return calculs;
        }

        [HttpGet("pie_chart/{id_user}")]
        public async Task<ActionResult<IEnumerable<Calculs>>> GetPieChart(int id_user)
        {
            CalculsType calcul_type = _context.CalculsType.First(c => c.name == "pie_chart");
            
            var calculs = _context.Calculs
                .Where(c => c.id_user == id_user)
                .Where(c => c.CalculsType == calcul_type)
                .ToList();

            foreach (var calcul in calculs)
            {
                var cat = _context.Category.First(c => c.id == calcul.CategoryId);

                calcul.Category = cat;
            }

            return calculs;
        }
    }
}