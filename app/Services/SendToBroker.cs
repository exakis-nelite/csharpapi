using System;
using System.Text;
using RabbitMQ.Client;

namespace app
{
    public class SendtoBroker
    {
        public void publish(string message, string queue)
        {
            string getEnv = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            var factory = new ConnectionFactory();

            if (getEnv == "production")
            {
                factory = new ConnectionFactory() { HostName = "35.242.155.183", Port = 32439, UserName="guest", Password="guest", VirtualHost = "/" };
            }
            if (getEnv == "development")
            {
                factory = new ConnectionFactory() { HostName = "rabbit1", Port = 5672, UserName="guest", Password="guest", VirtualHost = "/" };
            }
            if (getEnv == "gitlab")
            {
                factory = new ConnectionFactory() { HostName = "rabbitmq", Port = 5672, UserName="guest", Password="guest", VirtualHost = "/" };
            }
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: queue,
                    durable: true,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                //string message = "Hello World!";
                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(exchange: "",
                    routingKey: queue,
                    basicProperties: null,
                    body: body);
                
                Console.WriteLine(" [x] Sent {0}", message);
            }

            
        }
    }
}