const sonarqubeScanner = require('sonarqube-scanner');
const read = require('read-file');
const axios = require('axios');

async function httpGet(theUrl) {
    return axios.get(theUrl);
}

async function checkSonarReport() {
    const buffer = read.sync('.scannerwork/report-task.txt');
    const stringBuffer = buffer.toString();
    const ceTaskUrl = /(ceTaskUrl=)(.+)/.exec(stringBuffer)[2];
    
    let status;
    
   const interval =  await setInterval(async () => {
       const result = await httpGet(ceTaskUrl);
       if(result.data.task.status === "SUCCESS"){
            status = true;
            process.exit(0);
            clearInterval(interval);
        }else if(result.data.task.status === "FAILED"){
            status = false;
           process.exit(1);
            clearInterval(interval);
           
        }
    }, 1000);
}


sonarqubeScanner({
    serverUrl : "https://exakis-nelite.box-access.com",
    token: "3a2e17aafff69f6adb452155e319ac72107b83b4",
    options: {
        'sonar.inclusions': 'app/Program.cs, app/Controllers/**',
        'sonar.exclusions': 'node_modules/**',
        'sonar.projectKey': 'c_sharp_api',
        'sonar.test.inclusions': 'test/**'
    }
}, async () => {
    await checkSonarReport();
});
