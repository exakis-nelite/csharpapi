FROM ubuntu:bionic

LABEL dotnet-version="2.1.4"

RUN DEBIAN_FRONTEND=noninteractive apt-get update -y \
 && apt-get install -y net-tools \
                       iputils-ping \
                       curl \
                       wget \
                       unzip \
                       tzdata \
                       gnupg \
                       apt-transport-https \
 && wget -q https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb \
 && dpkg -i packages-microsoft-prod.deb \
 && sh -c "echo deb [arch=amd64] http://archive.ubuntu.com/ubuntu bionic universe >> /etc/apt/sources.list" \
 && sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/microsoft-ubuntu-artful-prod artful main" > /etc/apt/sources.list.d/dotnetdev.list' \
 && curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg \
 && mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg \
 && apt-get update 


RUN apt-get install -y dotnet-sdk-2.2

WORKDIR /app

# Copy csproj and restore as distinct layers

COPY ["app/app.csproj", "app/"] 
COPY ["test/test.csproj", "test/"] 

RUN dotnet restore app/app.csproj
RUN dotnet restore test/test.csproj

# Copy everything else and build
COPY . ./

RUN dotnet publish "app/app.csproj" -c Release -o /app 

RUN dotnet publish "test/test.csproj" -c Release -o /test 

WORKDIR /app
COPY /app .

WORKDIR /test
COPY /test /test 

WORKDIR /app

ENV ASPNETCORE_URLS http://*:5000
ENTRYPOINT ["dotnet"]